# Author Adam Wujek, CERN 2017

from test_config import *
from vme_men_helpers import *
import pytest
import time

vme = vme_men()

def test_check_status_register():
    assert vme.read_val(A24D32, CVORA_addr) == 0x02048601

def test_write_0x5a5a5a5a_to_channel_enable_mask_register():
    vme.write(A24D32, CVORA_addr + 0xc, value = 0x5a5a5a5a)
    val = vme.read_val(A24D32, CVORA_addr + 0xc)
    assert val == 0x5a5a5a5a

def test_write_0xa5a5a5a5_to_channel_enable_mask_register():
    vme.write(A24D32, CVORA_addr + 0xc, value = 0xa5a5a5a5)
    val = vme.read_val(A24D32, CVORA_addr + 0xc)
    assert val == 0xa5a5a5a5
