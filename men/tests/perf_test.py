import pytest
import time
from collections import namedtuple
from enum import IntEnum

from test_config import *
from vme_men_helpers import *
from vme_wrapper_helpers import *
from svec_common import *


def perf_local(vme, mode, space, addr, size, ffile, verify, retries):
    val = vme.perf(mode, space, addr = addr, size = size, ffile = ffile, verify = verify, retries = retries, mmap = False)
    return val


SVEC_spaces = [
    A24D16,
    A24D16_DMA,
    A24D16_BLT,
    A24D32,
    A24D32_DMA,
    A24D32_BLT,
    A24D64_MBLT,
    A32D32,
    A32D32_DMA,
    A32D32_BLT,
    A32D64_MBLT
]

RW_modes = [READ, WRITE]

SVEC_base_addr = {
    A24D16: SVEC_A24_ADDR,
    A24D16_DMA: SVEC_A24_ADDR,
    A24D16_BLT: SVEC_A24_ADDR,
    A24D32: SVEC_A24_ADDR,
    A24D32_DMA: SVEC_A24_ADDR,
    A24D32_BLT: SVEC_A24_ADDR,
    A24D64_MBLT: SVEC_A24_ADDR,
    A32D32: SVEC_A32_ADDR,
    A32D32_DMA: SVEC_A32_ADDR,
    A32D32_BLT: SVEC_A32_ADDR,
    A32D64_MBLT: SVEC_A32_ADDR,
}

min_size_map = {
    A24D16: 2,
    A24D16_DMA: 2,
    A24D16_BLT: 2,
    A24D32: 4,
    A24D32_DMA: 4,
    A24D32_BLT: 4,
    A24D64_MBLT: 8,
    A32D32: 4,
    A32D32_DMA: 4,
    A32D32_BLT: 4,
    A32D64_MBLT: 8,
}

SIS3320_spaces = [
    A32D32,
    A32D32_DMA,
    A32D32_BLT,
    A32D64_MBLT
]

SIS3320_base_addr = {
    A32D32: SIS3320_addr + SIS3320_buff_offset,
    A32D32_DMA: SIS3320_addr + SIS3320_buff_offset,
    A32D32_BLT: SIS3320_addr + SIS3320_buff_offset,
    A32D64_MBLT: SIS3320_addr + SIS3320_buff_offset,
}

CVORA_spaces = [
    A24D32,
    A24D32_DMA,
    A24D32_BLT,
]

CVORA_addr = 0x300020
CVORA_base_addr = {
    A24D32: CVORA_addr,
    A24D32_DMA: CVORA_addr,
    A24D32_BLT: CVORA_addr,
}

VME_Card = namedtuple('VME_Card', ["spaces", "modes", "base_addrs", "max_size", "set_space", "card_report"])

cards = {
#        Name                   spaces             RW               addr       size set_space
       "SVEC": VME_Card(   SVEC_spaces, [READ, WRITE],    SVEC_base_addr,  256*1024,     True, ""),
    "SIS3320": VME_Card(SIS3320_spaces,        [READ], SIS3320_base_addr, 1024*1024,    False, ""),
      "CVORA": VME_Card(  CVORA_spaces,        [READ],   CVORA_base_addr, 0x80000-0x20, False, ""),
}

#print(cards)


vme = vme_wrapper()


for card in cards:

    report = ""
    header =      "            | "
    header_line = "------------+-"

    print(card)
    spaces = cards.get(card).spaces
    modes = cards.get(card).modes
    base_addrs = cards.get(card).base_addrs
    max_size = cards.get(card).max_size
    set_space = cards.get(card).set_space
    card_report = cards.get(card).card_report
    size = 8
    header_len = 0
    while size <= max_size:
        header_line += "---------"
        header += "%8d " % size
        header_len += 9
        size *= 2
    header_line += "-+"
    header += " |"

    header_card = "            | "
    header_len -= len(card)
    i = 0
    while i < header_len/2:
        header_card += " "
        i += 1
    header_len -= i
    header_card += card
    while 0 < header_len + 1:
        header_card += " "
        header_len -= 1
    header_card += "|"

    for mode in modes:
        report += "%11s | " % mode
        size = 8
        while size <= max_size:
            report += "         "
            size *= 2
        report += " |\n"
        for space in spaces:
            size = 8
            base_addr = base_addrs.get(space, -1)
            if set_space == True:
                vme.slave_set_space(slot = SVEC_slot, space = space, ader = base_addr)
            report += "%11s | " % space
            while size <= max_size:
                if mode == READ:
                    ffile = None
                if mode == WRITE:
                    ffile = RANDOM_FILE
                retries = int(RANDOM_SIZE) / size
                #print(retries)
                retries = min(retries, 100)
                val = perf_local(vme, mode, space, addr = base_addr, size = size, ffile = ffile, verify = True, retries = retries)


                #print(val)
                report += "%8.3f " % val

                size *= 2
            report += " |\n"
    print (header_line)
    print (header_card)
    print (header_line)
    print (header)
    print (header_line)
    print (report.rstrip())
    print (header_line)

