# Author Adam Wujek, CERN 2017

from test_config import *
from helpers import *

import subprocess
import pytest

class vme_wrapper(vme_common):

    def vme_do(self, mode = None, space = None, addr = None, size = None, data_width = None, value = None, extra_cmd = None, mmap = False, retries = None, ffile = None, verify = None, buff_offset = None, dump_data = False, dma = None, novmeinc = False):
        if value == None:
            value = 0
        if mode == WRITE:
            mode = "-w 0x%x" % value
        if mode == READ:
            mode = "-r"
        data_width = self.get_data_width(space, data_width)
        if size == None:
            size = data_width

        spc_am_map = {
            A16D16:	0x29,
            A16D16_DMA:	0x29,
            A16D32:	0x29,
            A16D32_DMA:	0x29,
            A24D16:	0x39,
            A24D16_DMA:	0x39,
            A24D16_BLT:	0x3B,
            A24D32:	0x39,
            A24D32_DMA:	0x39,
            A24D32_BLT:	0x3B,
            A24D64_MBLT:0x38,
            A32D32:	0x09,
            A32D32_DMA:	0x09,
            A32D32_BLT:	0x0B,
            A32D64_MBLT:0x08,
            CRCSR:	0x2f,
            CRCSRD8:	0x2f,
            CRCSRD16:	0x2f,
            CRCSRD32:	0x2f
        }
        am = spc_am_map.get(space, 0)

        cmd = "sudo %s 0x%x 0x%x %s -a 0x%x -d %d" % (VME_BIN, addr, size, mode, am, data_width * 8)
        if space in (A24D16_BLT, A24D32_BLT, A24D64_MBLT, A32D32_BLT, A32D64_MBLT) or self.is_dma_space(space, dma):
            cmd += " -M"
        if verify == True:
            cmd += " -c"
        if ffile != None:
            cmd += " -f %s" % ffile
        if retries != None:
            cmd += " -n %d" % retries
        if dump_data == False:
            cmd += " -x"
        if novmeinc == True:
            cmd += " -i"
        if extra_cmd != None:
            cmd += " %s" % extra_cmd
        print_cmd (cmd)

        assert addr != None, "No VME address specified"
        assert data_width != None, "No data width nor address space specified"
        assert mode != None,  "No mode (Read/Write) specified"
        assert am != 0, "No address modifier specified"

        process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, error = process.communicate()
        rc = process.returncode
        check_return_code(rc, output, error)
        return rc, output, error

    def read(self, space = None, addr = None, size = None, data_width = None, extra_cmd = None, mmap = None, ffile = None, retries = None, dump_data = False, dma = None, novmeinc = False):
        rc, output, error = self.vme_do(mode = READ, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, retries = retries, dump_data = dump_data, dma = dma, novmeinc = novmeinc)
        return rc, output, error

    def read_val(self, space = None, addr = None, size = None, data_width = None, extra_cmd = None, mmap = None, dma = None):
        rc, output, error = self.vme_do(mode = READ, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, dump_data = True, dma = dma)
        data_width = self.get_data_width(space, data_width) # make sure data_width is valid; even when only space is provided
        output_dec = output.decode('utf-8').split("\n");
        data_index = output_dec.index("----") + 1
        #value_split = output.split()
        # get the line with the value
        value_split = output_dec[data_index].split()
        # get the value
        value_list = value_split[1:data_width+1]
        value = int("0x" + "".join(value_list), 16)
        return value

    def write(self, space = None, addr = None, size = None, data_width = None, value = None, extra_cmd = None, mmap = None, ffile = None, verify = None, retries = None, buff_offset = None, dump_data = False, dma = None):
        self.vme_do(mode = WRITE, space = space, addr = addr, data_width = data_width, size = size, value = value, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, verify = verify, retries = retries, buff_offset = buff_offset, dump_data = dump_data, dma = dma)
