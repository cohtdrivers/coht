# Author Adam Wujek, CERN 2017

import pytest
import time
from test_config import *
from vme_men_helpers import *
from svec_common import *

vme = vme_men()

# ------------------ CR/CSR -----------------
def test_check_CR_signature_from_CR_CSR_mmap():
    assert (vme.read_val(CRCSRD32, SVEC_CRCSR_addr + 0x1c, mmap = True) & 0xff) == 0x43
    assert (vme.read_val(CRCSRD32, SVEC_CRCSR_addr + 0x20, mmap = True) & 0xff) == 0x52

def test_svec_create_random_file():
    check_return_code(*exec_cmd("dd if=/dev/urandom of=" + RANDOM_FILE + " bs="+RANDOM_SIZE+" count=1"))

# ------------------ A24D16 -----------------
class Test_svec_A26D16_mmap(object):
    def test_svec_set_A24D16_mmap_mode(self):
        vme.slave_set_space(SVEC_slot, A24D16, SVEC_A24_ADDR)

    # using addr_fixtureD16-size_fixtureD16 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD16-addr_fixtureD16 data is replaced at every address change)
    def test_rw(self, size_fixtureD16, addr_fixtureD16):
        addr = addr_fixtureD16
        size = size_fixtureD16
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr)) + " size " + str(hex(size)))
        vme.write(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True, mmap = True)
        # verify write with a non-mmap read
        vme.read(A24D16, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        # -s silent; -n compate n bytes
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_rw_long(self):
        # long test
        vme.write(A24D16, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, mmap = True, retries = 10)

# ------------------ A24D32 -----------------
class Test_svec_A24D32_mmap(object):
    def test_svec_set_A24D32_mmap_mode(self):
        vme.slave_set_space(SVEC_slot, A24D32, SVEC_A24_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at every address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr
        print_cmd ("address " + str(hex(SVEC_A24_ADDR + addr)) + " size " + str(hex(size)))
        vme.write(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True, mmap = True)
        # verify write with a non-mmap read
        vme.read(A24D32, addr = SVEC_A24_ADDR + addr, size = size, ffile = TMP_FILE)
        # -s silent; -n compate n bytes
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))

    def test_rw_long(self):
        # long test
        vme.write(A24D32, addr = SVEC_A24_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, mmap = True, retries = 10)

# ------------------ A32D32 -----------------
class Test_svec_A32D32_mmap(object):
    def test_svec_set_A32D32_mmap_mode(self):
        vme.slave_set_space(SVEC_slot, A32D32, SVEC_A32_ADDR)

    # using addr_fixtureD32-size_fixtureD32 is more logical, but worse from testing point of view
    # (because new data is added at the end, replaced only on address change,
    #  using size_fixtureD32-addr_fixtureD32 data is replaced at every address change)
    def test_rw(self, size_fixtureD32, addr_fixtureD32):
        addr = addr_fixtureD32
        size = size_fixtureD32
        if addr + size > SVEC_MEM_SIZE:
            size = SVEC_MEM_SIZE - addr
        print_cmd ("address " + str(hex(SVEC_A32_ADDR + addr)) + " size " + str(hex(size)))
        vme.write(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = RANDOM_FILE, verify = True, mmap = True)
        # verify write with a non-mmap read
        vme.read(A32D32, addr = SVEC_A32_ADDR + addr, size = size, ffile = TMP_FILE)
        # -s silent; -n compate n bytes
        check_return_code(*exec_cmd("cmp -s -n " + str(size) + " " + RANDOM_FILE + " " + TMP_FILE))


    def test_rw_long(self):
        ## long test
        vme.write(A32D32, addr = SVEC_A32_ADDR, size = SVEC_MEM_SIZE, ffile = RANDOM_FILE, verify = True, mmap = True, retries = 10)

