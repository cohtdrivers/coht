# Author Adam Wujek, CERN 2017

from test_config import *
from vme_men_helpers import *
import pytest
import time

vme = vme_men()

def test_check_CR_signature_from_CR_CSR():
    assert vme.read_val(CRCSRD32, VD80_addr + 0x1c) == 0xffffff43
    assert vme.read_val(CRCSRD32, VD80_addr + 0x20) == 0xffffff52

def test_write_0xa5_to_CR_CSR():
    # it does not write this value...
    vme.write(CRCSRD32, VD80_addr + 0x7ffd0, value = 0x5a5a5a5a)
    val = vme.read_val(CRCSRD32, VD80_addr + 0x7ffd0)
    assert val == 0xffffff5a

def test_write_0x5a_to_CR_CSR():
    # it does not write this value...
    vme.write(CRCSRD16, VD80_addr + 0x7ffd2, value = 0xa5a5)
    val = vme.read_val(CRCSRD32, VD80_addr + 0x7ffd0)
    assert val == 0xffffffa5
    val = vme.read_val(CRCSRD16, VD80_addr + 0x7ffd2)
    assert val == 0xffa5
