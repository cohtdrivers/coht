# Author Adam Wujek, CERN 2017

from test_config import *
from helpers import *

import subprocess

class vme_men(vme_common):
    def vme_do(self, mode = None, space = None, addr = None, size = None, data_width = None, value = None, extra_cmd = None, mmap = False, retries = None, ffile = None, verify = None, buff_offset = None, dump_data = False, dma = None, novmeinc = False):
        if value == None:
            value = 0
        if mode == WRITE:
            mode = "-w -V=0x%x" % value
        if mode == READ:
            mode = "-r"
        data_width = self.get_data_width(space, data_width)
        if size == None:
            size = data_width
        spc_map = {
            A16D16: 0,
            A16D16_DMA: 0,
            A16D32: 2,
            A16D32_DMA: 2,
            A24D16: 4,
            A24D16_DMA: 4,
            A24D16_BLT: 5,
            A24D32: 6,
            A24D32_DMA: 6,
            A24D32_BLT: 7,
            A24D64_MBLT: 1,
            A32D32: 8,
            A32D32_DMA: 8,
            A32D32_BLT: 9,
            A32D64_MBLT: 10,
            CRCSR: 30,
            CRCSRD8: 30,
            CRCSRD16: 30,
            CRCSRD32: 30
        }

        spc = spc_map.get(space, -1)

        cmd = "sudo %s 0x%x 0x%x -a=%d %s -s=%d" % (VME_RWEX_BIN, addr, size, data_width, mode, spc)
        if mmap == True:
            cmd += " -m"
        if verify == True:
            cmd += " -c"
        if ffile != None:
            cmd += " -f=%s" % ffile
        if retries != None:
            cmd += " -n=%d" % retries
        if buff_offset != None:
            cmd += " -k=0x%x" % buff_offset
        if self.is_dma_space(space, dma):
            cmd += " -t"
        if novmeinc == True:
            cmd += " -i"
        if dump_data == False:
            cmd += " -d"
        if extra_cmd != None:
            cmd += " %s" % extra_cmd

        print_cmd (cmd)

        assert addr != None, "No VME address specified"
        assert data_width != None, "No data width nor address space specified"
        assert mode != None,  "No mode (Read/Write) specified"
        assert spc >= 0, "No address modifier specified"

        process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, error = process.communicate()
        rc = process.returncode
        check_return_code(rc, output, error)
        return rc, output, error

    def read(self, space = None, addr = None, size = None , data_width = None, extra_cmd = None, mmap = None, ffile = None, retries = None, dump_data = False, dma = None, novmeinc = False):
        rc, output, error = self.vme_do(mode = READ, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, retries = retries, dump_data = dump_data, dma = dma, novmeinc = novmeinc)
        return rc, output, error

    def read_val(self, space = None, addr = None, size = None, data_width = None, extra_cmd = None, mmap = None, ffile = None, dma = None):
        rc, output, error = self.vme_do(mode = READ, space = space, addr = addr, data_width = data_width, size = size, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, dump_data = True, dma = dma)
        data_width = self.get_data_width(space, data_width) # make sure data_width is valid; even when only space is provided
        output_dec = output.decode('utf-8').split("\n");
        data_index = output_dec.index("----") + 1
        # get the line with the value
        value_split = output_dec[data_index].split()
        # get the value
        value_list = value_split[1:data_width+1]
        value = int("0x" + "".join(value_list), 16)
        return value

    def write(self, space = None, addr = None, size = None, data_width = None, value = None, extra_cmd = None, mmap = None, ffile = None, verify = None, retries = None, buff_offset = None, dump_data = False, dma = None):
        self.vme_do(mode = WRITE, space = space, addr = addr, data_width = data_width, size = size, value = value, extra_cmd = extra_cmd, mmap = mmap, ffile = ffile, verify = verify, retries = retries, buff_offset = buff_offset, dump_data = dump_data, dma = dma)

    def slave_set_space(self, slot, space, ader):
        CRCSR_addr = slot * 0x80000
        fun_ader = None
        am = None
        if space in (A24D16, A24D16_DMA, A24D16_BLT, A24D32, A24D32_DMA, A24D32_BLT, A24D64_MBLT):
            fun_ader = 1
        if space in (A32D32, A32D32_DMA, A32D32_BLT, A32D64_MBLT):
            fun_ader = 0

        if space in (A24D16, A24D16_DMA, A24D32, A24D32_DMA):
            am = 0x39
        if space in (A24D16_BLT, A24D32_BLT):
            am = 0x3b
        if space == A24D64_MBLT:
            am = 0x38
        if space in (A32D32, A32D32_DMA):
            am = 0x9
        if space == A32D32_BLT:
            am = 0xb
        if space == A32D64_MBLT:
            am = 0x8

        assert slot > 0, "Wrong slot number"
        assert fun_ader != None, "Wrong address space %s" % space
        assert am != None, "Wrong address space %s" % space
        cmd = "sudo %s -w -s=%d -f=%x -a=0x%x -v=0x%x" % (VME_CRCSR_BIN, slot, fun_ader, am, ader)
        print_cmd (cmd)
        process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, error = process.communicate()
        rc = process.returncode
        check_return_code(rc, output, error)

        cmd = "sudo %s -r -s=%d -f=%x" % (VME_CRCSR_BIN, slot, fun_ader)
        print_cmd (cmd)
        process = subprocess.Popen(cmd, shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        output, error = process.communicate()
        rc = process.returncode
        check_return_code(rc, output, error)

        output_dec = output.decode('utf-8').split(" ");
        assert int(output_dec[1], 0) == slot, "Slot not set correctly"
        assert int(output_dec[3], 0) == ader, "Ader not set correctly"
        assert int(output_dec[5], 0) == am, "AM not set correctly"
        assert int(output_dec[7], 0) == fun_ader, "un_ader not set correctly"
