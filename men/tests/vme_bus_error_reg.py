# Author Adam Wujek, CERN 2017

from vme_men_helpers import *

def test_bus_error_io_error():
    cmd = """sudo """ + VME_RWEX_BIN + " " + str(hex(BUS_ERROR_addr)) + """ 0x100 -a=4 -w -s=6 -v=0 2>&1 | cut -d$'\\n' -f 6-"""
    result = """Input/output error\n"""
    assert cmp_result(cmd, result)

def test_bus_error_ber_clean():
    cmd = """sudo """ + VME_CTRL_BIN + " ber """
    #ignore result for the first call, since we don't know the previous state
    exec_cmd(cmd)
    result = """No valid or new Bus error info available since last call to VME4L_BusErrorGet().\n"""
    # use the same command as before
    assert cmp_result(cmd, result)

def test_bus_error_present():
    #make sure ber is clean
    test_bus_error_ber_clean()
    # generate bus error
    test_bus_error_io_error()
    # read the bus error
    cmd = """sudo """ + VME_CTRL_BIN + " ber """
    result = """Bus error info is valid. Reason: write to VME addr 0x2c0000 AM 0x39 normal state.\n"""
    assert cmp_result(cmd, result)
