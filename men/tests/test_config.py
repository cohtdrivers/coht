VME_RWEX_BIN="acc/local/$CPU/drv/vmebus_a25/1.0.0/bin/vme4l_rwex"
VME_CTRL_BIN="acc/local/$CPU/drv/vmebus_a25/1.0.0/bin/vme4l_ctrl"
VME_CRCSR_BIN="acc/local/$CPU/drv/vmebus_a25/1.0.0/bin/vme4l_crcsr"
VME_BIN="acc/local/$CPU/drv/vmebus/1.0.1/bin/vmea"
VMEIO_BIN="acc/local/$CPU/drv/rawio/1.0/bin/vmeio"
CTRTEST_BIN="acc/dsc/none/$CPU/ctr/2.2/bin/ctrtest"
CVORATEST_BIN="acc/local/$CPU/drv/cvora/1.2/bin/cvoratest.py"
CVORBTEST_BIN="acc/local/$CPU/drv/cvorb/1.0/bin/cvorbtest"
SISTEST_BIN="acc/local/$CPU/drv/sis33/1.1.0/bin/sis33test"

cfg_print_cmd=""
# uncomment to print executed commands
cfg_print_cmd="y"

cfg_print_error=""
# uncomment to print executed commands
cfg_print_error="y"


CTRV_addr=0xc00000
CVORA_addr=0x300000
CVORB_addr=0x400
# address depends on the rotary switch settings
SIS3320_addr=0x10000000
SIS3320_buff_offset=0x4000000
# address depends on a slot possition
VD80_addr=0x700000

# address is calculated based on a slot number
SVEC_slot=10
SVEC_A32_ADDR=0x5000000
SVEC_A24_ADDR=0x0500000

# address to test bus error, no card should be at this address
BUS_ERROR_addr=0x2c0000

FUN0ADER=0x7FF60
FUN1ADER=0x7FF70
