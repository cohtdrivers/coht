# Author Adam Wujek, CERN 2017

from test_config import *
from vme_men_helpers import *
import pytest
import time

vme = vme_men()

def test_check_VHDL_synthesis_date():
    assert vme.read_val(A24D32, CTRV_addr+20) == 0x4d06177a

def test_check_RT_Second_register_not_0():
    assert vme.read_val(A24D32, CTRV_addr+72) != 0

def test_check_RT_Second_register_increases():
    val1 = vme.read_val(A24D32, CTRV_addr+72)
    time.sleep(1)
    val2 = vme.read_val(A24D32, CTRV_addr+72)
    assert val1 != val2

def test_write_0xa5a5a5a5_to_Delay1_register_24_bits_rw():
    vme.write(A24D32, CTRV_addr + 208, value = 0xa5a5a5a5)
    val = vme.read_val(A24D32, CTRV_addr + 208)
    assert val == 0x00a5a5a5

def test_write_0x5a5a5a5a_to_Delay1_register_24_bits_rw():
    vme.write(A24D32, CTRV_addr + 208, value = 0x5a5a5a5a)
    val = vme.read_val(A24D32, CTRV_addr + 208)
    assert val == 0x005a5a5a

def test_write_0xa5a5a5a5_to_Config1_register_10_bits_rw():
    vme.write(A24D32, CTRV_addr + 204, value = 0xa5a5a5a5)
    val = vme.read_val(A24D32, CTRV_addr + 204)
    assert val == 0xa5800000
