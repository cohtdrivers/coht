#!/bin/bash

# (C) CERN 2018
# author: Adam Wujek <adam.wujek@cern.ch>

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#"

TRANSFERS_LIST=transfers_list.txt

if ! [ -e "$TRANSFERS_LIST" ]; then
    echo "Generating transfer.ref's list"
    "$SCRIPT_DIR"/list_transfers.sh > "$SCRIPT_DIR"/"$TRANSFERS_LIST"
fi

hosts=`cat "$SCRIPT_DIR"/"$TRANSFERS_LIST"`

#echo $hosts
for line in $hosts; do
#    echo $line
    if grep "MEN-A25" $line &>/dev/null; then
#        echo "$line"
        IFS='/'
        arr=($line)
        IFS=' '
        host=${arr[4]}
        echo -n "$host "
        ssh -tt -q $host "sudo /usr/local/bin/fpga_load_a25 1a88 4d45 d5 0 -n | grep gateware-revision: | cut -d \" \" -f 2 " 2>/dev/null
        ret=$?
        if [ $ret -ne 0 ]; then
            echo "Unable to connect"
        fi
#        exit
    fi
done
