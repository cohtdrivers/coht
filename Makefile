export REPO_PARENT := $(shell pwd)
include common.mk
include defconfig.mk
CONFIG_FILE ?= config.mk
-include $(CONFIG_FILE)

ifeq ($(CONFIG_FPGA_MGR_BACKPORT),n)
CONFIG_fpga_mgr = n
endif

DIRS-y =
DIRS-$(CONFIG_acdx) += acdx
DIRS-$(CONFIG_ctc) += ctc
DIRS-$(CONFIG_ctr) += ctr
DIRS-$(CONFIG_cvora) += cvora
DIRS-$(CONFIG_cvorb) += cvorb
DIRS-$(CONFIG_cvorg) += cvorg
DIRS-$(CONFIG_encore) += encore
DIRS-$(CONFIG_fmc) += fmc
DIRS-$(CONFIG_fmc_adc_100m14b4cha) += fmc-adc-100m14b4cha/software
DIRS-$(CONFIG_fmc_delay_1ns_8cha) += fmc-delay-1ns-8cha/software
DIRS-$(CONFIG_fmc_sw) += fmc-sw
DIRS-$(CONFIG_fmc_tdc) += fmc-tdc/software
DIRS-$(CONFIG_fpga_mgr) += fpga-mgr-backport
DIRS-$(CONFIG_general_cores) += general-cores/software
DIRS-$(CONFIG_icv196) += icv196
DIRS-$(CONFIG_ipack) += ipack
DIRS-$(CONFIG_jtag) += jtag
DIRS-$(CONFIG_men) +=
DIRS-$(CONFIG_mil1553) += mil1553
DIRS-$(CONFIG_mockturtle) += mockturtle
DIRS-$(CONFIG_mtt) += mtt
DIRS-$(CONFIG_pciioconfig) += pciioconfig
DIRS-$(CONFIG_pickering_lxi) += pickering-lxi
DIRS-$(CONFIG_pickeringmux) += pickeringmux
DIRS-$(CONFIG_rawio) += rawio
DIRS-$(CONFIG_sis33) += sis33
DIRS-$(CONFIG_soft_cpu_toolchains) += soft-cpu-toolchains
DIRS-$(CONFIG_spec) += spec/software
DIRS-$(CONFIG_svec) += svec/software
DIRS-$(CONFIG_vd80) += vd80
DIRS-$(CONFIG_vmebridge_ng) += vme-bridge/software
DIRS-$(CONFIG_vmod) += vmod
DIRS-$(CONFIG_zio) += zio


.PHONY: all clean cleanall install $(DIRS-y) init_submodules init_submodules_post init_submodules_pre

all clean cleanall install: $(DIRS-y)
clean: TARGET = clean
cleanall: TARGET = cleanall
install: TARGET = install

$(DIRS-y):
	$(MAKE) -C $@ $(TARGET)

# dependencies
pickeringmux: pciioconfig

ctr cvorb cvorg fmc-adc-100m14b4cha/software icv196 ipack jtag mtt sis33 svec/software vd80 vmod: vme-bridge/software

# ctc needs encore
ctc: encore

# cvora needs encore
cvora: encore

fmc-adc-100m14b4cha/software fmc-delay-1ns-8cha/software fmc-tdc/software: fmc-sw zio

spec/software svec/software: fmc-sw
ifeq ($(CONFIG_FPGA_MGR_BACKPORT),y)
spec/software svec/software: fpga-mgr-backport
endif

# init_submodules rule is used by CI machines (or by any other user) to be able
# to clone submodules from ohwr.org on machines without internet connecton.
# NOTE: It removes existing submodules!
init_submodules: init_submodules_post

# Define which machine to use to get submodules
GIT_GATEWAY=cs-ccr-builds

# Define all submodules that shall be cloned. To simplify we don't distinguish
# between CERN and outside repos.
# We cannot simply parse all .gitmodules, because for fmc's repos we don't want
# to clone recursively all submodules (for example we keep only one copy of
# fmc-bus).
# Nested submodules are supported, but require "init" for module which has
# nested submodules
SUBMODULE_LIST+='(acdx			"ssh://git@gitlab.cern.ch:7999/cohtdrivers/acdx.git")'
SUBMODULE_LIST+='(mil1553		"ssh://git@gitlab.cern.ch:7999/cohtdrivers/mil1553.git")'
SUBMODULE_LIST+='(ctr-2.0		"ssh://git@gitlab.cern.ch:7999/cohtdrivers/ctr-2.0.git")'
SUBMODULE_LIST+='(encore		"ssh://git@gitlab.cern.ch:7999/cohtdrivers/encore.git")'
SUBMODULE_LIST+='(fmc/adc-lib		"git://ohwr.org/misc/adc-lib.git")'
SUBMODULE_LIST+='(fmc/zio		"git://ohwr.org/misc/zio.git")'
SUBMODULE_LIST+='(fmc/fmc-bus		"git://ohwr.org/fmc-projects/fmc-bus.git")'
SUBMODULE_LIST+='(fmc/spec-sw		"git://ohwr.org/fmc-projects/spec/spec-sw.git")'
SUBMODULE_LIST+='(fmc/svec-sw		"git://ohwr.org/fmc-projects/svec/svec-sw.git")'
SUBMODULE_LIST+='(fmc/fine-delay-sw	"git://ohwr.org/fmc-projects/fmc-delay-1ns-8cha/fine-delay-sw.git")'
SUBMODULE_LIST+='(fmc/fmc-adc-100m14b4cha-sw "git://ohwr.org/fmc-projects/fmc-adc-100m14b4cha/fmc-adc-100m14b4cha-sw.git")'
SUBMODULE_LIST+='(fmc/fmc-tdc-sw	"git://ohwr.org/fmc-projects/fmc-tdc/fmc-tdc-sw.git")'
SUBMODULE_LIST+='(fmc/obsbox		"ssh://git@gitlab.cern.ch:7999/cohtdrivers/obsbox.git")'
SUBMODULE_LIST+='(ipack			"ssh://git@gitlab.cern.ch:7999/cohtdrivers/ipack.git")'
SUBMODULE_LIST+='(mtt			"ssh://git@gitlab.cern.ch:7999/cohtdrivers/mtt.git")'
SUBMODULE_LIST+='(ctc			"ssh://git@gitlab.cern.ch:7999/cohtdrivers/ctc.git")'
SUBMODULE_LIST+='(cvora			"ssh://git@gitlab.cern.ch:7999/cohtdrivers/cvora.git")'
SUBMODULE_LIST+='(soft-cpu-toolchains	"git://ohwr.org/misc/soft-cpu-toolchains.git")'
SUBMODULE_LIST+='(men/menlinux		"ssh://git@gitlab.cern.ch:7999/mendrivers/menlinux.git"		"init")'  # perform init of submodules
SUBMODULE_LIST+='(men/menlinux/TOOLS/MDIS_API		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_tools_mdis_api.git")'
SUBMODULE_LIST+='(men/menlinux/INCLUDE/COM/MEN		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_include_com_men.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/CHAMELEON/COM	"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_chameleon_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/DESC/COM		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_desc_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/ID/COM		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_id_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/MBUF/COM		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_mbuf_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/PLD/COM		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_pld_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/USR_UTL/COM	"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_usr_utl_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/SMB/PORT/COM	"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_smb_port_com.git")'
SUBMODULE_LIST+='(men/menlinux/LIBSRC/USR_OSS		"ssh://git@gitlab.cern.ch:7999/mendrivers/mdis_libsrc_usr_oss.git")'
SUBMODULE_LIST+='(men/men-wrapper	"ssh://git@gitlab.cern.ch:7999/cohtdrivers/men-wrapper.git")'
SUBMODULE_LIST+='(men/men-dma-kernel-test	"ssh://git@gitlab.cern.ch:7999/cohtdrivers/men-dma-kernel-test.git")'
SUBMODULE_LIST+='(fmc-sw ssh://git@ohwr.org:7999/project/fmc-sw.git)'
SUBMODULE_LIST+='(fpga-mgr-backport ssh://git@gitlab.cern.ch:7999/coht/fpga-manager.git)'
SUBMODULE_LIST+='(general-cores ssh://git@ohwr.org:7999/project/general-cores.git)'
SUBMODULE_LIST+='(spec ssh://git@ohwr.org:7999/project/spec.git)'
SUBMODULE_LIST+='(svec ssh://git@ohwr.org:7999/project/svec.git)'

init_submodules_pre:
# empty
	

init_submodules_all: init_submodules_pre
#	Create tempdir and save its name
	$(eval TEMP_OHWR_DIR := $(shell ssh $(GIT_GATEWAY) "sh -c \"mktemp -d ohwr.XXXXXXXXXX --tmpdir\""))
	@echo "Using temporary directory $(TEMP_OHWR_DIR) on git gateway ($(GIT_GATEWAY))"
#	Clone submodules on GIT_GATEWAY and init submodules (needed for nested submodules).
#	Please note that we remove repo from the place where we want to copy new one.
#	If we don't remove it rsync may/will mess up in .git
	-for d in $(SUBMODULE_LIST); do \
	    declare -a arrayofrepos=$$d;\
	    path=$${arrayofrepos[0]};\
	    url=$${arrayofrepos[1]};\
	    echo "Removing $$path if present";\
	    rm -rf $$path;\
	    echo "Cloning repo $$url to $$path";\
	    ssh $(GIT_GATEWAY) "cd $(TEMP_OHWR_DIR) && git clone -- \"$$url\" \"$$path\"" \
	    || exit 1; \
	    done

#	Transfer all cloned repos to the local machine
	-rsync -a $(GIT_GATEWAY):$(TEMP_OHWR_DIR)/* .
#	remove temporary directory on $(GIT_GATEWAY)
	ssh $(GIT_GATEWAY) "rm -rf $(TEMP_OHWR_DIR)"

init_submodules_post: init_submodules_all
#	initialize submodules (non recursive)
	git submodule init
	git submodule update
#	Checkout correct commit in copied repos
	for d in $(SUBMODULE_LIST); do \
	    declare -a arrayofrepos=$$d;\
	    path=$${arrayofrepos[0]};\
	    other_params=$${arrayofrepos[2]};\
	    if [ "$$other_params" == init ]; then pushd "$$path" && git submodule update --init && popd || exit 1; fi ;\
	    done
	git submodule update --recursive --checkout
