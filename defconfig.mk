# Use CONFIG_ macros, even if we are not using Kconfig. Use "y".
# All of these can be set to =n in the environment to disable building
# the respective module

CONFIG_acdx ?= y
CONFIG_ctc ?= y
CONFIG_ctr ?= y
CONFIG_ctsyn ?= y
CONFIG_cvora ?= y
CONFIG_cvorb ?= y
CONFIG_cvorg ?= y
CONFIG_encore ?= y
CONFIG_fmc ?= y
CONFIG_fmc_adc_100m14b4cha ?= y
CONFIG_fmc_delay_1ns_8cha ?= y
CONFIG_fmc_sw ?= y
CONFIG_fmc_tdc ?= y
CONFIG_fpga_mgr ?= y
CONFIG_general_cores ?= y
CONFIG_icv196 ?= y
CONFIG_ipack ?= y
CONFIG_jtag ?= y
CONFIG_men ?= y
CONFIG_mil1553 ?= y
CONFIG_mockturtle ?= y
CONFIG_mtt ?= y
CONFIG_pciioconfig ?= y
CONFIG_pickering_lxi ?= y
CONFIG_pickeringmux ?= y
CONFIG_rawio ?= y
CONFIG_sis33 ?= y
CONFIG_soft-cpu-toolchains ?= n # build manually if you need it because
				# it takes too much time
CONFIG_spec ?= y
CONFIG_svec ?= y
CONFIG_vd80 ?= y
CONFIG_vmebridge_ng ?= y
CONFIG_vmod ?= y
CONFIG_zio ?= y
