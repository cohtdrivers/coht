#!/bin/bash

SPEC_APP_RE="(golden|fmc-adc|fine-delay|fmc-tdc)"
SVEC_APP_RE="(golden|fmc-adc|fine-delay|fmc-tdc)"
VERSION_RE="v[0-9]+\.[0-9]+\.[0-9]+(\.rc[0-9]+)?"

for ENV in "lab" "oper"
do
    echo "The following SPEC bitstreams in /acc/local/share/firmware/$ENV have an invalid name"
    ls -1 /acc/local/share/firmware/$ENV/ | \
        grep -E "^spec-.*" | \
        grep -E -v "spec-${SPEC_APP_RE}-${VERSION_RE}-(45|100|150)t.bin"
done

for ENV in "lab" "oper"
do
    echo "The following SVEC bitstreams in /acc/local/share/firmware/$ENV have an invalid name"
    ls -1 /acc/local/share/firmware/$ENV/ | \
        grep -E "^svec-.*" | \
        grep -E -v "svec-${SPEC_APP_RE}-${VERSION_RE}.bin"
done
