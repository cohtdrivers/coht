#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 CERN (home.cern)
#
# SPDX-License-Identifier: LGPL-2.1-or-later

import argparse
import os
import logging
from pathlib import Path
from urllib.request import urlretrieve
from shutil import copytree, copy, rmtree
from tempfile import TemporaryDirectory
import tarfile

proprietary_list = ["spectrum"]

name_translator = {
    "spectrum": "spcm",
}


def project_dst_dir_get(project_name):
    if project_name in name_translator:
        return name_translator[project_name]
    return project_name


def project_src_dir_get(project_name):
    if project_name in name_translator:
        return "usr/"
    return "usr/local"


def target_get_prefix(target):
    if target == "deb12x64":
        return "debian/12/x86_64"
    return target


def filename_build(args, name, version, distro, suffix=""):
    if distro == "debian/12/x86_64":
        target = "fecos_2023"
    else:
        target = distro
    return args.file_str_format.format(name=name,
                                       target=target,
                                       version=version) + suffix


def tarfile_download(args, download_dir):
    filename = filename_build(args, ".tar.xz")
    dst = os.path.join(download_dir, filename)
    url = f"https://be-cem-edl.web.cern.ch/{args.project}/{args.version}/software/{filename}"
    logging.info(f"Downloading '{url}' into '{dst}'")
    path, header = urlretrieve(url, dst)
    return path


def tarfile_retrive_path(args):
    version = args.version
    release = Path(args.tar_source).joinpath(args.project).joinpath(version)
    if not release.is_dir():
        version = f"v{args.version}"
        release = Path(args.tar_source).joinpath(args.project).joinpath(version)
    if not release.is_dir():
        logging.error(f"Can't file release {args.project} {args.version}")
        sys.exit(1)

    return release.joinpath("software").joinpath(filename_build(args,
                                                                args.project,
                                                                version,
                                                                args.distro,
                                                                ".tar.xz"))


def tarfile_get(args, workdir):
    if args.from_http:
        return tarfile_download(args, workdir)
    return tarfile_retrive_path(args)


def tarfile_unpack(tarfilepath, destination):
    if not tarfile.is_tarfile(tarfilepath):
        raise ValueError(f"Invalid tar file {tarfilepath}")

    with tarfile.open(tarfilepath, mode='r:*') as tar:
        tarfilename = os.path.basename(tarfilepath)
        dirname = os.path.splitext(os.path.splitext(tarfilename)[0])[0]
        dst = os.path.join(destination, dirname)
        logging.info(f"Extracting '{tarfilepath}' into '{dst}'")

        tar.extractall(path=dst)
    return dst


def version_maj_min_link(directory, version):
    link = Path(directory).joinpath('.'.join(version.split(".")[:2]))
    if link.exists():
        logging.warn("Path {rel_link} already exists but it is not a symbolic link ")
    else:
        # symlinks do not exists as files
        if link.is_symlink():
            link.unlink()
        link.symlink_to(version)


def project_has_drivers(project_name, project_dir):
    path = os.path.join(project_dir, "usr/lib/modules")
    return os.path.exists(path)


def project_has_userspace(project_name, project_dir):
    return True


def install_user(args, name, version, project_dir, destdir):
    if not project_has_userspace(name, project_dir):
        logging.info(f"No userspace found for {name} {version}")
        return
    prefix = "acc/local"
    distro = target_get_prefix(args.distro)
    dst_prj = os.path.join(destdir, prefix, distro,
                           "drv", project_dst_dir_get(name))
    dst = os.path.join(dst_prj, version)
    src = os.path.join(project_dir, project_src_dir_get(name))
    logging.info(f"Copy userspace files from '{src}' into '{dst}'")
    if os.path.exists(dst):
        logging.error(f"Destination directory {dst} already exists, can't override")
    else:
        copytree(src, dst, symlinks=True)
        for so in Path(dst).glob("**/*.so"):
            print(so)
            if so.is_symlink():
                so.unlink()
        # Remove kernel directories if any
        moddir = os.path.join(dst, "usr/lib/modules")
        if os.path.exists(moddir):
            rmtree(moddir)
    version_maj_min_link(dst_prj, version)


def install_driver(args, name, version, project_dir, destdir):
    if not project_has_drivers(name, project_dir):
        logging.info(f"No Linux modules found for {name} {version}")
        return

    prefix = "acc/dsc"
    distro = target_get_prefix(args.distro)

    dst = os.path.join(destdir, prefix, args.accenv, distro)
    src = os.path.join(project_dir, "usr/lib/modules")
    logging.info(f"Copy Linux kernel drivers from '{src}' into '{dst}'")
    copytree(src, dst, dirs_exist_ok=True, symlinks=True)

    kver = [k for k in Path(src).iterdir()][0].name
    dst_prj = os.path.join(dst, kver, project_dst_dir_get(name))
    version_maj_min_link(dst_prj, version)


def install(args, project_dir, destdir):
    if args.version.startswith("v"):
        version = args.version[1:]
    else:
        version = args.version

    install_user(args, args.project, version, project_dir, destdir)
    install_driver(args, args.project, version, project_dir, destdir)


def main(args_in=None):
    if "LOG_LEVEL" in os.environ:
        log_level = os.environ["LOG_LEVEL"]
    else:
        log_level = "WARNING"

    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % log_level)
    logging.basicConfig(level=numeric_level,
                        format='%(asctime)s drvdeployer::%(levelname)s %(message)s')

    mainparser = argparse.ArgumentParser()
    mainparser.add_argument('--workdir', default="/tmp",
                            help="Working directory")
    mainparser.add_argument('--destdir', default="",
                            help="Destination directory (Default to local directory)")
    mainparser.add_argument('--file-str-format',
                            default="{name}-software-{target}-{version}",
                            help="Default format to build filenames")
    mainparser.add_argument('--tar-source', default="/eos/project/b/be-cem-edl/www/",
                            help="Provide an alternative source path for tar files on the local filesystem")
    mainparser.add_argument('--from-http', default=False, action="store_true",
                            help="Instead of using files on EOS, download them from HTTP")
    mainparser.add_argument('accenv', choices=["lab", "oper", "oplhc"],
                            help="Accelerator deployment environment")
    mainparser.add_argument('distro', choices=["L867", "debian/12/x86_64", "deb12x64"],
                            help="Target distribution")
    mainparser.add_argument('project',
                            help="Project to be deployed")
    mainparser.add_argument('version',
                            help="Project version")
    args = mainparser.parse_args(args_in)

    # Standardize to version without "v"
    if args.version.startswith("v"):
        args.version = args.version[1:]

    with TemporaryDirectory("", "drvdeployer-",
                            args.workdir) as workdir:
        logging.debug(f"Working directory: {workdir}")
        tarfilepath = tarfile_get(args, workdir)
        dirpath = tarfile_unpack(tarfilepath, workdir)
        install(args, dirpath, args.destdir)


if __name__ == "__main__":
    main()
